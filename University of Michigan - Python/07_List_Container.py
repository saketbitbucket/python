#1 List  => mutable ordered collection of elements , order is remembered
# list uses []
#  list remembers the order in which elements are inserted 

from pickle import TRUE


mylist = [1,2,3,4,5,6]
print(mylist)

mixedtypelist = ["MixedType list element 1",2,3.5,4,5.0]
print(mixedtypelist)

print("--------------------iterating the list--------------")
for element in mixedtypelist:
    print(element)

#creating empty list andappending data
print("------working with empty [] as list----")
emptyList = []
emptyList.append(10)
emptyList.append(20)
emptyList.append(30)
print(emptyList)

#creating empty list using ctor
print("-----------creating empty list using ctor------------")
listctor = list()
listctor.append(-1)
listctor.append(-2)
listctor.sort() #sort list in default order i.e accending
print(listctor)
listctor.sort(reverse = True) #sort list in reverse order i.e decending
print(listctor)

# iteratinng list using index
print("-------------iterating list using index and while loop------------")

lst = [1,11,111,1111,11111,111111,1111111,111111111,1111111111,11111111111]

lstlen = len(lst)

while lstlen > 0 :
    index = len(lst) - lstlen
    print(lst[index])
    lstlen  -= 1

#iterating list using index and for loop

print("-------------------iterating list using index and for loop-------")
lis = [1111111111,111111111,11111111, 1111111, 111111, 11111, 1111, 111, 11, 1]
for index in range(len(lis)):
    print(lis[index])
     

print("-----------------------Modifying all values in the list---------------")
intlist = [1,2,3,4,5]
for item in intlist:
    print(item)

print("afetr modification")
for index in range(len(intlist)):
    intlist[index] *= 5
    print(intlist[index])

