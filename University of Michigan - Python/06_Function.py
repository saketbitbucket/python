def ping():
    return "Ping!"

print(ping())

def sphereVolume(r):
    """ Return volume of sphere """ # it is dark string
    v = (4.0/3.0) * 3.142* r**3
    return v

print("volume of sphre 2  " , sphereVolume(2))

def unitConvertorCM(feet =0, inches =0):
    """  convert from feet to inches"""
    inches_to_cm = inches * 2.54
    feet_to_cm = feet * 12 *2.54
    return inches_to_cm + feet_to_cm
      
print("testing default/keyword argument function")

print("unitConvertorCM(feet = 5)", unitConvertorCM(feet = 5))
print("unitConvertorCM(inches = 70)", unitConvertorCM(inches = 70))
print("unitConvertorCM(feet = 5, inches = 8)", unitConvertorCM(feet = 5, inches = 8))
print("unitConvertorCM(inches = 8, feet = 5)", unitConvertorCM(inches = 8, feet = 5))