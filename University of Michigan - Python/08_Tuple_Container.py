# tuple is an immutable and efficient list 
# tuple uses ()
# we can't change any value in tuple we can only itrerate
# tuple remembers the order like list does
from datetime import date

print("---------------------Empty tuple --------------------------")
empty_tuple = ()
print(empty_tuple)

print("---------------------merge two tuple using + --------------------------")
empty_tuple += (1, "1", 2.0)
print(empty_tuple)

print("---------------------merge two tuple using + --------------------------")
empty_tuple += (1, "1", 2.0)
print(empty_tuple)

print("------------tuple of ints------")
mytuple = (1,2,3,4,5)
for item in mytuple:
    print(item)

print("------------tuple of mixed type------")
today = date.today
mytuple = (1,"2",3.0,today)
for item in mytuple:
    print(item)


print("------------accessing tuple using index------")
for index in range(len(mytuple)):
    print("value at index {} is {}".format(index, mytuple[index]))