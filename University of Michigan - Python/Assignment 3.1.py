#3.1 Write a program to prompt the user for hours and rate per hour using input to compute gross pay. Pay the hourly rate for the hours up to 40 and 1.5 times the hourly rate for all hours worked above 40 hours. Use 45 hours and a rate of 10.50 per hour to test the program (the pay should be 498.75). You should use input to read a string and float() to convert the string to a number. Do not worry about error checking the user input - assume the user types numbers properly.

#hour = input("input prompt : Enter hours ")
#rate = input("input prompt : Enter the rate ")
#
#if float(hour) <= 40:
#    pay = float(hour) * float(rate)
#    
#else :
#    pay = (40 * float(rate) )+((float(hour) - 40) * float(rate) * 1.5)
#
#print(pay)

# improve ments to above code

hour = input("input prompt : Enter hours ")
rate = input("input prompt : Enter the rate ")

try:
   fhr = float(hour)
   frate = float(rate)
except:
   print("Invalid data entered")
   quit()

if fhr <= 40:
    pay = fhr * frate
    
else :
    pay = (40 * frate ) + ((fhr - 40) * frate * 1.5)

print(pay)