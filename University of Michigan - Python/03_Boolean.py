print("True is " , type(True))
print("False is " , type(False))

print("int of true is ", int(True))
print("int of false is ", int(False))

print (" 5+true : " , 5 + True)
print (" 10 * false : " , 10 * False)

print ("bool of 66 ", bool(66))
print ("bool of -6 ", bool(-6))
print ("bool of 0 ", bool(0))

a = True

print (" a = True  , a is :" , type(a))