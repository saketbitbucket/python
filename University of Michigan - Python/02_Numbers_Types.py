print("Numbers and types in python 2 and python 3 are different")
print("Python 3 does not have a long type. Instead, int itself allows large values (limited only by available memory) in effect, Python 2\'s  long was renamed to int.")

print("Int \n.........................")

a = 469

print("priniting intiger : ", a)

b = 7613458931 ; c = 645326 ; d = 908   #one way to declare multiple variable on same line

print("priniting intigers :  a = ", a, "b = ",b, "c = ",c)

x ,y, z = 7931 , 5326, 8   #one way to declare multiple variable on same line

print("priniting intigers :  x= ", x, "y = ",y, "z = ",z)


print("Float \n......................")

pi1 = 3.142

pi2 = 22/7

print ("pi value ", pi1)

print ("pi by division ", pi2)

print("\n...............................\nComplex Number 2 -6j Note python uses j for imaginary part ") #python uses j for imagrinary part

complex = 2 -6.1j

print(type(complex))

print( "real part is " , complex.real)
print( "imag part is " , complex.imag)



