class Employee:
    countofObjects = 0

    def __init__(self,fname, lname, pay): #instance method uses self as first argument
        self.firstName = fname
        self.lastName = lname
        self.pay = pay
        Employee.countofObjects += 1

    @classmethod
    def instanceCount(cls):   # @classmethod uses cls as first argument
        return cls.countofObjects
     
    @staticmethod
    def areEmployeesWorking(day):
        if day == 6 or day == 7:
           return "Yes"
        else:
           return "No"

emp_1 = Employee("A","1", 100)
emp_2 = Employee("B","2", 200)
emp_3 = Employee("C","3", 300)
emp_4 = Employee("D","4", 400)
emp_5 = Employee("E","5", 500)


print("-----------------------------------------------------------")
print("id of emp_1 object is ",id(emp_1) , "\n")
print("-----------------------------------------------------------")
print("Total objects created = " , Employee.countofObjects)
print("\n---------------------------------------------------------\n")
print("Does employess work on Tuesay = {} and Saturday = {}".format(Employee.areEmployeesWorking(2), Employee.areEmployeesWorking(6)))
print("\n---------------------------------------------------------\n")
