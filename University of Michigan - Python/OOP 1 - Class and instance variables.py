class Employee:
    
    Hike = 0.104 # class variable for yearly hike

    def __init__(self, fname, lname, pay):
        #crating instance variable and initilizing thme
        self.firstName = fname
        self.lastName = lname
        self.email = fname + lname + "@company.com"
        self.pay = pay

    def EmployeeDetails(self):
        return "Name : {} {}, Email: {} , Pay: {}".format(self.firstName, self.lastName,self.email, self.pay)  

    
    def GiveRaise(self):
        self.pay = self.pay + (self.pay*self.Hike)



employee_1 = Employee("chals", "seveanse", 100000)
employee_2 = Employee("Moush","youtube", 100000)
employee_3 = Employee("SK","programmer", 120000)
employee_4 = Employee("Python","Programming", 150000)

emp_manager = Employee("Moush","youtube", 250000)

#------------------------------------------------------------------
#Important concept is we can override class variable for an instance
# for example set hike for manager to 20%
emp_manager.Hike = 0.20 # this will add a new instance variable 
#-----------------------------------------------------------------

print(employee_1.__dict__)
print("  ")
print(emp_manager.__dict__) #see the hike instance variable
print("  ")
print(Employee.__dict__)
print("  ")
print("EMPLOYEE DETAILS: \n employee_1.EmployeeDetails()--> {} \n".format(employee_1.EmployeeDetails() ))
print("  ")
print("Employee.EmployeeDetails(employee_1)--> \n" , Employee.EmployeeDetails(employee_1))
print("  ")

employee_1.GiveRaise()

print("Employee 1 after raise ", employee_1.__dict__)