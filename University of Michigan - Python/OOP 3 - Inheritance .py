class Employee:
    def __init__(self, fname, lname):
        self.FirstName = fname  #instance variables
        self.LastName = lname   #instance variables
    

class Engineer(Employee):
    def __init__(self, fname, lname, skill):
        #super().__init__(fname, lname) --> if super is used no need to pass self
        Employee.__init__(self,fname,lname) #if using class naemwe need to pass self
        self.skilledAt = skill
    
    def EngineerProfile(self):
        return "Name : {} {} , {} engineer".format(self.FirstName, self.LastName, self.skilledAt )



print("-------------------------------------------------------------------------\n\n")
engineer_1 = Engineer("E", "1", "Software")
engineer_2 = Engineer("E", "2", "Electrical")
engineer_3 = Engineer("E", "3", "Hardware")
engineer_4 = Engineer("E", "4", "Build")


print(engineer_1.EngineerProfile())
print(engineer_2.EngineerProfile())
print(engineer_3.EngineerProfile())
print(engineer_4.EngineerProfile())
