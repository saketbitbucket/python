# set is unique set of elements which doesn't remember order
#  set also uses {} but no : as its element are not in pair and not uses :

#creating empty set

# myset = {} --> will create dictionary
myset = set()

myset.add(1)
myset.add(11)
myset.add(111)
myset.add(1) # this is duplicate
myset.add(1111)

print(myset)

print("------------using loop-----------")
for value in myset:
    print(value)

print("""------------create set from string "AABBCC" -> string is added char by char-----------""")

myset = set("AABBCC")
print(myset)

print("""------------create set from list --> list is added element by element -----------""")

myset = set([ "this", "is", "set" ,"yes", "it" , "is", "total 7" ])
print(myset)

