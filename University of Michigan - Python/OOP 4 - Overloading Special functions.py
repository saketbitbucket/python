#special functions in python are surrounded by __ and __
#it is also called as dunder

class Employee:
    def __init__(self, fname, lname, address):
        self.firstName = fname
        self.lastName = lname
        self.address = address

    def __str__(self):
        return "Name : {} {} , address {}".format(self.firstName, self.lastName, self.address)
      
    def __len__(self):
        return len(self.firstName) + len(self.lastName)


e1 = Employee("SK", "Kr", "MI, USA")
print("-----------------------------------------------------------------------------\n")

print(id(Employee), "  ", e1, "\n")
