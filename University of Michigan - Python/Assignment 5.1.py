#5.2 Write a program that repeatedly prompts a user for integer numbers until the user enters 'done'. Once 'done' is entered, print out the largest and smallest of the numbers. If the user enters anything other than a valid number catch it with a try/except and put out an appropriate message and ignore the number. Enter 7, 2, bob, 10, and 4 and match the output below.


#understand the use of None we can use only is and isnot with None not = or != operator

#largest = -1
#smallest = 100
#
#inp1 = None
#while inp1 != "done":
#    inp1 = input("Enter interger or done  - ")
#    if inp1 != "done":
#        inp1 = int(inp1)
#        try:
#            inp1 = int(inp1)
#            if largest < inp1 :
#                largest = inp1
#            if smallest > inp1 :
#               smallest = inp1
#        except:
#            print("Invalid input")
#print("Maximum is", largest)
#print("Minimum is", smallest)
    
largest = None
smallest = None
while True:
    num = input("Enter a number: ")
    if num == "done":
        break
    try:
        num = int(num)  
        if largest is None:
           largest = num
            
        if smallest is None:
           smallest = num
            
        if largest < num:
            largest = num
            
        if smallest > num:
            smallest = num
    except:
        print("Invalid input")

print("Maximum is", largest)
print("Minimum is", smallest)

