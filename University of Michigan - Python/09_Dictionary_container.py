# dictionary is a accociative data sture which accosiates a key with a value and constraint is no key is duplicated
# dictionary uses {}

import datetime

print("------Dictionary of all string in key and value--------")
mydictionary = {"key 1": "value 1", "key 2": "2", "key 3" : "3" }
print(mydictionary)

print("------Dictionary with mixed data type in key and value--------")
dateofbirth = datetime.date(1991, 4, 25)
mixeddictionary = {"Name": "saket", "Age" : 30, "height" : 5.8, "date of birth": dateofbirth}
print(mixeddictionary)

print("------iterating over Dictionary with for loop give acceess to keys only--------")
for key in mixeddictionary:
    print(key)

print("------iterating over Dictionary.items() using key and value--------")
for key,value in mixeddictionary.items():
    print(key,":", value)

print("----------accessing and modifing the values in the dictionary")
intdict = {"item1" : 1, "item2" : 2, "item3" : 3, "item4" : 4, "item5" : 5 }
for key in intdict:
    print(intdict[key])

print("multiplying to all values by 5 using get() function")
for key in intdict:
    intdict[key] = intdict.get(key, 0) * 5
    print(intdict[key])


