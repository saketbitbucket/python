
from types import LambdaType


class Date:
    def __init__(self, day, month, year):
       self.day = day
       self.month = month
       self.year = year
    
    def __str__(self):
        return "{} /{} /{}".format(self.day, self.month, self.year)


class Employee:
      def __init__(self, fname, lname, dayOfJoining, monthofJoing, yearofJoining):
          self.FirstName = fname
          self.LastName = lname
          self.DateOfJoing = Date(dayOfJoining, monthofJoing, yearofJoining) # date class instance is used to hold the date of joining

      def __str__(self):
          return " {} {} joined on {}".format(self.FirstName, self.LastName, self.DateOfJoing) #format function converts instance to string


emp1 = Employee("Nick","jonas",7,2,2022 )

print("--------------------------------------------------")
print(emp1)
print("\n")