#9.4 Write a program to read through the mbox-short.txt and figure out who has sent the greatest number of mail messages. The program looks for 'From ' lines and takes the second word of those lines as the person who sent the mail. The program creates a Python dictionary that maps the sender's mail address to a count of the number of times they appear in the file. After the dictionary is produced, the program reads through the dictionary using a maximum loop to find the most prolific committer.

filename = input("Enter file:")
if len(filename) < 1:
    filename = "mbox-short.txt"
fh = open(filename)

senders = dict()
for line in fh:
    if not line.startswith("From "):
        continue
    words = line.split()
    if len(words) > 1:
        senders[words[1]] = senders.get(words[1], 0) + 1

max = 0
maxWord = None
for word,count in senders.items():
    if count > max:
        maxWord = word
        max = count
print(maxWord,max)