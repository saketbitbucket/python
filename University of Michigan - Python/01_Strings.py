message1 = "Hello World"
print(message1)

message2 = "Hello World string using double quotes syntax" #used double quotes
print(message2)

message3 = 'Hello world using single quotes syntax'
print(message3)

message4 = " Hello World \"with double quotes \" syntax"  #used escape seuence to display quotes as alphabets not syntax chars
print(message4)

message5 = 'Helo world \' with single quotes\' syntax'
print(message5)

message6 = 'hello world "using single quotes" syntax'  #used mix and match of double and single quotes
print(message6)

message7 = "hello world 'using double quotes' syntax"
print(message7)