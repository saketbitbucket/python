from ProbStatModule import ComputeHistogramFrequency

print("In the client script using the prob stat module")

#notice no dot nottation with module.function required since we used from...import syntax
print(ComputeHistogramFrequency([1,3,4,5,6,1,5,2,3,4,6,5,2,6,6,6,3,2,1,4,2,5,4,6,2,3]))
