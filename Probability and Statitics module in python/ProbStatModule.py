import math
# function expects list of numbers and returns a dictionary with intigers as key freequencies as values.
def ComputeHistogramFrequency(numbers):
    frequencies = dict()
    for number in numbers:
        frequencies[number] = frequencies.get(number,0)+1
    return frequencies

def ComputeRelativeFreequencies(numbers):
    frequencies = ComputeHistogramFrequency(numbers)
    totalcount = 0
    for key in frequencies:
        totalcount = totalcount + frequencies[key]
    for key in frequencies :
        frequencies[key] = frequencies[key] / totalcount
    return frequencies


def NcR(n , r):
    nFactorial = math.factorial(n)
    rFactorial = math.factorial(r)
    nMinusrFact = math.factorial(n-r)
    return nFactorial / (nMinusrFact * rFactorial)

def NpR(n , r):
    nFactorial = math.factorial(n)
    nMinusrFact = math.factorial(n-r)
    return nFactorial / nMinusrFact
